// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#include "MK64F12.h"                         // K64F M4 register definitions


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
#define Pot          0x12                    // Potentiometer is on A/D 1 Channel 18
#define nSW2         (GPIOC_PDIR&(1<<6))     // Switch 1, negative logic (0=pressed, 1=not pressed)
#define nSW3         (GPIOA_PDIR&(1<<4))     // Switch 2, negative logic (0=pressed, 1=not pressed), also the NMI pin


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint32_t adcin;                              // adcin will store ADC results
uint8_t cin;


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(uint32_t delay);              // delay in multiples of 1ms

//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);  // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint32_t ADC_Read(uint8_t chan);             // Analog-to-Digital Converter byte read: enter channel to read
//---------------------------------------------------------------------
void UART_Init(void);                        // initializes the serial port
void UART_CharOut(uint8_t data);             // sends out a character
void UART_NibbOut(uint8_t data);             // sends out a nibble (hex value)
void UART_ByteOut(uint8_t data);             // sends out a byte (hex value)
void UART_TextOut(char *data);               // sends out a string
uint8_t  UART_CharIn(void);                  // reads a character
uint8_t  UART_ByteIn(void);                  // reads two characters and interprets them as a byte (hex)
void UART_CRLF(void);                        // send a carriage return + line feed


// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
UART_Init();                                 // also initialize the UART 
RGB(0,0,0);
for(;;)                                      // forever loop 
  {
  cin=UART_CharIn();                         // read a character from the UART (probably from your PC)
  RGB(1,0,0);                                // Flash RED LED when key is pressed
  MCU_Delay(5); 
  RGB(0,0,0);                                
  if (cin=='!')                              // if the character is a '!',
    UART_TextOut("K64F Sample UART0 Code");  // then send out some text,
  else                                       // otherwise,
    {
    UART_CharOut(cin);                       // show the character,
    UART_CharOut('=');                       // and also,
    UART_ByteOut(cin);                       // the ASCII value in hex
    }
  UART_CRLF();                               // go to the next line                   
  } 
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Crucial                       
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
WDOG_UNLOCK = WDOG_UNLOCK_WDOGUNLOCK(0xC520); // Key 1
WDOG_UNLOCK = WDOG_UNLOCK_WDOGUNLOCK(0xD928); // Key 2
WDOG_STCTRLH &= ~WDOG_STCTRLH_WDOGEN_MASK;    // Disable Watchdog


//System Registers 
SIM_SCGC5|=SIM_SCGC5_PORTA_MASK;             // Enable PortA clock
SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;             // Enable PortB clock (1 GPIO)
SIM_SCGC5|=SIM_SCGC5_PORTC_MASK;             // Enable PortC clock
SIM_SCGC5|=SIM_SCGC5_PORTD_MASK;             // Enable PortD clock (2 GPIO)
SIM_SCGC5|=SIM_SCGC5_PORTE_MASK;             // Enable PortE clock
SIM_SCGC4|=SIM_SCGC4_UART0_MASK;             // Enable UART0 clock
SIM_SCGC3|=SIM_SCGC3_ADC1_MASK;              // Enable ADC1 module


//System clock initialization
MCG_C1=0x04;                                 // FEI mode, internal clock 32.768KHz
MCG_C4|=0x80;                                // Bus Clock is 24MHz, 32,768KHz x 732 = 24MHz

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=24000;                         // Busclk/1=32.768KHz*732=24000000Hz, Period=1ms/(1/24000000Hz)=24000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock) 

// ADC Init
ADC1_CFG2=0x10;                              // Long sample time, ADCxxB channels selected 
ADC1_CFG1=0x40;                              // mode 8-bit(0x40), 10-bit(0x48), 12-bit(0x44), busclk/4 = 5.24MHz
ADC1_SC2=0x00;                               // DMA disabled, VREH & VREF pins selected
ADC1_SC3=0x00;                               // Single Conversion, Continuous Conversion is (0x08)
ADC1_SC1A=0x12;                              // Set to ADC0 channel 18, if Continuous Conversion mode selected, ADC0_SC3 MUST BE WRITTEN TO LAST
			 
		 
// GPIO Init
PORTB_PCR16=PORT_PCR_MUX(3);                 // Set Pin B16 to alt UART0_RX function
PORTB_PCR17=PORT_PCR_MUX(3);                 // Set Pin B17 to alt UART0_TX function
PORTC_PCR6=PORT_PCR_MUX(1)|0x03;             // SW2 is now a GPIO, with pullup enabled (negative logic)(External Shield Brd)
PORTA_PCR4=PORT_PCR_MUX(1)|0x03;             // SW3 is now a GPIO, with pullup enabled (negative logic)(External Shield Brd)
PORTA_PCR14=PORT_PCR_MUX(1)|0x03;            // ACCEL INT1 is now a GPIO, with pullup enabled
PORTB_PCR22=PORT_PCR_MUX(1);                 // Set Pin B22 to GPIO function
PORTE_PCR26=PORT_PCR_MUX(1);                 // Set Pin E26 to GPIO function
PORTB_PCR21=PORT_PCR_MUX(1);                 // Set Pin B21 to GPIO function
GPIOB_PDDR|=(1<<22);                         // Set Pin B22 as output
GPIOE_PDDR|=(1<<26);                         // Set Pin E26 as output
GPIOB_PDDR|=(1<<21);                         // Set Pin B21 as output
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue) // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB_PCOR|=(1<<22); else GPIOB_PSOR|=(1<<22);
if (Green ==1) GPIOE_PCOR|=(1<<26); else GPIOE_PSOR|=(1<<26);
if (Blue  ==1) GPIOB_PCOR|=(1<<21); else GPIOB_PSOR|=(1<<21);
}
//---------------------------------------------------------------------
uint32_t ADC_Read(uint8_t chan)              // Analog-to-Digital Converter byte read: enter channel to read
{ADC1_SC1A=chan; while (!(ADC1_SC1A&ADC_SC1_COCO_MASK)); return ADC1_RA;}
// UART FUNCTIONS //////////////////////////////////////////////////////
//---------------------------------------------------------------------
void UART_Init(void)          {UART0_C2=0x00; UART0_C4=0x0D; UART0_BDH=0x00; UART0_BDL=0x0D; UART0_C1=0x00; UART0_S2=0x00; UART0_C3=0x00; UART0_C2=0x0C;}   // 20.97MHz/14/13=115,200 baud (actually 115,228, HEX 0x0D, OSR=14, HEX 0x0E)
//---------------------------------------------------------------------
void UART_CharOut(uint8_t data)   {while ((UART0_S1&UART_S1_TDRE_MASK)==0); UART0_D=data;}
//---------------------------------------------------------------------
void UART_NibbOut(uint8_t data)   {if (data>0x09) UART_CharOut(data+0x37); else UART_CharOut(data+0x30);}
//---------------------------------------------------------------------
void UART_ByteOut(uint8_t data)   {UART_NibbOut(data>>4); UART_NibbOut(data&0x0F);}
//---------------------------------------------------------------------
void UART_WordOut(uint16_t data)  {UART_ByteOut(data>>8); UART_ByteOut(data&0xFF);}
//---------------------------------------------------------------------
void UART_TextOut(char *data) {while (*data!=0x00) {UART_CharOut(*data); data++;}}
//---------------------------------------------------------------------
uint8_t UART_CharIn(void)         {while ((UART0_S1&UART_S1_RDRF_MASK)==0); return UART0_D;}
//---------------------------------------------------------------------
uint8_t UART_ByteIn(void)         {uint8_t h,l; h=UART_CharIn()-48; if (h>9) h-=7; l=UART_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
//---------------------------------------------------------------------
void UART_CRLF(void)          {UART_CharOut(0x0D); UART_CharOut(0x0A);}


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


