// INCLUDES /////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------     
#include "MK64F12.h"                           // K64F M4 register definitions

// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
//#define nSW1         (GPIOA_PDIR&(1<<4))     // Switch 1, negative logic (0=pressed, 1=not pressed)
//#define nSW3         (GPIOC_PDIR&(1<<1))     // Switch 3, negative logic (0=pressed, 1=not pressed)
//#define ADC_Chan     0x01                    // Connect POT wiper lead to FRDM-KL27Z board, J4-2


// GLOBAL CONSTANTS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
uint16_t adcin;                              // adcin will store ADC results


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(uint32_t delay);              // delay in multiples of 1ms

//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue);    // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
uint16_t ADC_Read(uint8_t chan);                     // Analog-to-Digital Converter int read: enter channel to read


// *** MAIN *********************************************************                    
//-------------------------------------------------------------------                    
int main(void)                                                                           
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0);                                  // Start with all LEDs off
for(;;)                                      // forever loop 
  { 
   RGB(1,0,0);                               // Turn Red LED On
   MCU_Delay(100);                           // Delay 100ms
   RGB(0,0,0);                               // Turn Red LED Off
   MCU_Delay(100);                           // Delay 100ms
  } 
// return 0;                                                                             
}                                                                                        
//-------------------------------------------------------------------                    
// ******************************************************************                    


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
void MCU_Init(void) 
{
// Crucial                       
__asm("CPSID i");                            // Disable interrupts

//Disable Watchdog
SIM_COPC=0x00;                               // Disable COP watchdog

//System Registers 
SIM_SCGC5|=SIM_SCGC5_PORTA_MASK;             // Enable PortA clock
SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;             // Enable PortB clock
SIM_SCGC5|=SIM_SCGC5_PORTC_MASK;             // Enable PortC clock
SIM_SCGC5|=SIM_SCGC5_PORTD_MASK;             // Enable PortD clock
SIM_SCGC5|=SIM_SCGC5_PORTE_MASK;             // Enable PortE clock

// System clock initialization
MCG_MC|=MCG_MC_HIRCEN_MASK;                  // Enable 48MHz HIRC
MCG_C1=MCG_C1_CLKS(0);                       // Enable Main Clock Source of the 48MHz HIRC
SIM_CLKDIV1&=~(SIM_CLKDIV1_OUTDIV1(1));      // Set Core Clock/1=48MHz 
SIM_CLKDIV1|=(SIM_CLKDIV1_OUTDIV4(1));       // Set Bus Clock/2=24MHz    

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=48000;                         // Core Clock/1=48MHz, Period=1ms/(1/48000000Hz)=48000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock) 

// GPIO Init
PORTB_PCR18=PORT_PCR_MUX(1);                 // Set Pin B18 to GPIO function
PORTB_PCR19=PORT_PCR_MUX(1);                 // Set Pin B19 to GPIO function
PORTA_PCR13=PORT_PCR_MUX(1);                 // Set Pin A13 to GPIO function
GPIOB_PDDR|=(1<<18);                         // Set Pin B18 as output
GPIOB_PDDR|=(1<<19);                         // Set Pin B19 as output
GPIOA_PDDR|=(1<<13);                         // Set Pin A13 as output
}
//---------------------------------------------------------------------
void MCU_Delay (uint32_t delay)              // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{uint32_t delw; for (delw=0;delw<delay;delw++) {while (!(SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk));}}
//---------------------------------------------------------------------
void RGB(uint8_t Red,uint8_t Green,uint8_t Blue) // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (Red   ==1) GPIOB_PCOR|=(1<<18); else GPIOB_PSOR|=(1<<18);
if (Green ==1) GPIOB_PCOR|=(1<<19); else GPIOB_PSOR|=(1<<19);
if (Blue  ==1) GPIOA_PCOR|=(1<<13); else GPIOA_PSOR|=(1<<13);
}
//---------------------------------------------------------------------
uint16_t ADC_Read(uint8_t chan)              // Analog-to-Digital Converter int read: enter channel to read
{ADC0_SC1A=chan; while (!(ADC0_SC1A&ADC_SC1_COCO_MASK)); return ADC0_RA;}


// INTERRUPT BODIES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


